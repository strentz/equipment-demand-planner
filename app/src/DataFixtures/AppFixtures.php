<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\Campervan;
use App\Entity\Equipment;
use App\Entity\Station;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $campervans = $this->createCampervans($manager);
        $equipments = $this->createEquipments($manager);
        $stations = $this->createStations($manager, $campervans, $equipments);

        for ($i = 0; $i < 10; $i++) {
            $booking = new Booking();
            $booking->setOrigin($this->pick($stations));
            $booking->setDestination($this->pick($stations));
            $booking->setCampervan($this->pick($campervans));

            $first = rand(1, 5);
            $last = rand($first + 1, $first + 10);

            $booking->setStartAt(new DateTime("+$first day"));
            $booking->setEndAt(new DateTime("+$last day"));

            foreach ($equipments as $equipment) {
                $bookingEquipment = new Booking\BookingEquipment();
                $bookingEquipment->setBooking($booking);
                $bookingEquipment->setEquipment($equipment);
                $bookingEquipment->setQuantity(rand(1, 5));

                $manager->persist($bookingEquipment);
            }

            $manager->persist($booking);
        }

        $manager->flush();
    }

    /**
     * @return Campervan[]
     */
    private function createCampervans(ObjectManager $manager): array
    {
        $names = ['Surfer Suite', 'Travel Home', 'Beach Hostel', 'Camper Cabin'];
        $campervans = [];

        foreach ($names as $name) {
            $campervan = new Campervan();
            $campervan->setName($name);
            $manager->persist($campervan);

            $campervans[] = $campervan;
        }

        return $campervans;
    }

    /**
     * @return Equipment[]
     */
    private function createEquipments(ObjectManager $manager): array
    {
        $names = ['Toilet', 'Sheets', 'Sleeping Bag', 'Chair'];
        $equipments = [];

        foreach ($names as $name) {
            $equipment = new Equipment();
            $equipment->setName($name);
            $manager->persist($equipment);

            $equipments[] = $equipment;
        }

        return $equipments;
    }

    /**
     * @return Station[]
     */
    private function createStations(ObjectManager $manager, array $campervans, array $equipments): array
    {
        $names = ['Munich', 'Paris', 'Porto', 'Madrid'];
        $stations = [];

        foreach ($names as $name) {
            $station = new Station();
            $station->setName($name);

            foreach ($campervans as $campervan) {
                $stationCampervan = new Station\StationCampervan();
                $stationCampervan->setCampervan($campervan);
                $stationCampervan->setStation($station);
                $stationCampervan->setQuantity(rand(0, 5));
                $manager->persist($stationCampervan);
            }

            foreach ($equipments as $equipment) {
                $stationEquipment = new Station\StationEquipment();
                $stationEquipment->setEquipment($equipment);
                $stationEquipment->setStation($station);
                $stationEquipment->setQuantity(rand(0, 5));
                $manager->persist($stationEquipment);
            }

            $manager->persist($station);

            $stations[] = $station;
        }

        return $stations;
    }

    /**
     * Choose a random element.
     *
     * @return mixed
     */
    private function pick(array $collection)
    {
        return $collection[array_rand($collection)];
    }
}
