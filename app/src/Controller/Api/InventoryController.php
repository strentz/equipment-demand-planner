<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Repository\StationRepository;
use App\Response\Api\SuccessResponse;
use App\Service\InventoryService;
use App\Validator\ValidationException;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/inventory")
 */
class InventoryController extends AbstractController
{
    private ValidatorInterface $validator;
    private InventoryService $inventoryService;

    public function __construct(ValidatorInterface $validator, InventoryService $inventoryService)
    {
        $this->validator = $validator;
        $this->inventoryService = $inventoryService;
    }

    /**
     * @Route("/{date}",
     *     methods={"GET"},
     *     requirements={"date"="^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$"},
     *     name="api_inventory"
     * )
     */
    public function inventory(string $date, StationRepository $stationRepository): Response
    {

        $errors = $this->validator->validate($date, new Date());

        if ($errors->count()) {
            throw new BadRequestException('Invalid resource identifier. It must be a valid date.');
        }

        $stations = $stationRepository->findAll();

        try {
            $invetory = $this->inventoryService->calculateInventory($stations, new DateTime($date));
        } catch (ValidationException $e) {
            throw new BadRequestException($e->getMessage());
        }

        return new SuccessResponse($invetory);
    }
}
