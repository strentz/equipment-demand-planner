<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Response\Api\SuccessResponse;
use App\Service\StationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StationController extends AbstractController
{
    /**
     * @Route("/api/stations",  methods={"GET"}, name="api_stations")
     */
    public function stations(StationService $stationService): Response
    {
        $stations = $stationService->getStations();

        return new SuccessResponse($stations);
    }
}
