<?php

declare(strict_types=1);

namespace App\Controller\Dashboard;

use App\Repository\StationRepository;
use App\Service\InventoryService;
use App\Validator\ValidationException;
use DateTime;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class IndexController extends AbstractController
{
    private ValidatorInterface $validator;
    private InventoryService $inventoryService;

    public function __construct(ValidatorInterface $validator, InventoryService $inventoryService)
    {
        $this->validator = $validator;
        $this->inventoryService = $inventoryService;
    }

    /**
     * @Route("/dashboard", methods={"GET"}, name="app_dashboard_index")
     */
    public function index(): Response
    {
        $now = new DateTime();

        return $this->redirectToRoute('app_dashboard', ['date' => $now->format('Y-m-d')]);
    }

    /**
     * @Route("/dashboard/{date}",
     *     methods={"GET"},
     *     requirements={"date"="^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$"},
     *     name="app_dashboard"
     * )
     */
    public function dashboard(string $date, StationRepository $stationRepository): Response
    {
        $errors = $this->validator->validate($date, new Date());

        if ($errors->count()) {
            throw new NotFoundHttpException('Not Found');
        }

        $stations = $stationRepository->findAll();
        $forecastAt = new DateTimeImmutable($date);

        try {
            $invetory = $this->inventoryService->calculateInventory($stations, $forecastAt);
        } catch (ValidationException $e) {
            throw new NotFoundHttpException('Not Found');
        }

        return $this->render('dashboard/index.html.twig', [
            'stations' => $invetory->getStations(),
            'forecastAt' => $forecastAt
        ]);
    }
}
