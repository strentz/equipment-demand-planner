<?php

declare(strict_types=1);

namespace App\Repository\Station;

use App\Entity\Station\StationCampervan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StationCampervan|null find($id, $lockMode = null, $lockVersion = null)
 * @method StationCampervan|null findOneBy(array $criteria, array $orderBy = null)
 * @method StationCampervan[]    findAll()
 * @method StationCampervan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationCampervanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StationCampervan::class);
    }
}
