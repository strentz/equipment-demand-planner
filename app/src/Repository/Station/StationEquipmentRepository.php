<?php

declare(strict_types=1);

namespace App\Repository\Station;

use App\Entity\Station\StationEquipment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StationEquipment|null find($id, $lockMode = null, $lockVersion = null)
 * @method StationEquipment|null findOneBy(array $criteria, array $orderBy = null)
 * @method StationEquipment[]    findAll()
 * @method StationEquipment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationEquipmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StationEquipment::class);
    }
}
