<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CampervanInventory;
use App\Entity\Station;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampervanInventory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampervanInventory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampervanInventory[]    findAll()
 * @method CampervanInventory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampervanInventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampervanInventory::class);
    }

    public function findBetweenRange(Station $station, DateTimeInterface $startAt, DateTimeInterface $endAt): array
    {
        $equipmentInventory = [];
        $rows = $this->createQueryBuilder('I')
                     ->select('IDENTITY(I.campervan) AS campervanId')
                     ->addSelect('SUM(I.inboundQuantity) AS inboundQuantity')
                     ->addSelect('SUM(I.outboundQuantity) AS outboundQuantity')
                     ->where('I.station = :station')
                     ->andWhere('I.forecastAt BETWEEN :startAt AND :endAt')
                     ->groupBy('I.campervan')
                     ->setParameter('startAt', $startAt->format('Y-m-d'))
                     ->setParameter('endAt', $endAt->format('Y-m-d'))
                     ->setParameter('station', $station)
                     ->getQuery()
                     ->getResult();

        foreach ($rows as $row) {
            $data = array_map('intval', $row);
            $index = $data['campervanId'];
            $equipmentInventory[$index] = $data;
        }

        return $equipmentInventory;
    }
}
