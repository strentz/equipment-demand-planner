<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\EquipmentInventory;
use App\Entity\Station;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipmentInventory|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipmentInventory|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipmentInventory[]    findAll()
 * @method EquipmentInventory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentInventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentInventory::class);
    }

    public function findBetweenRange(Station $station, DateTimeInterface $startAt, DateTimeInterface $endAt): array
    {
        $equipmentInventory = [];
        $rows = $this->createQueryBuilder('I')
                     ->select('IDENTITY(I.equipment) AS equipmentId')
                     ->addSelect('SUM(I.inboundQuantity) AS inboundQuantity')
                     ->addSelect('SUM(I.outboundQuantity) AS outboundQuantity')
                     ->where('I.station = :station')
                     ->andWhere('I.forecastAt BETWEEN :startAt AND :endAt')
                     ->groupBy('I.equipment')
                     ->setParameter('startAt', $startAt->format('Y-m-d'))
                     ->setParameter('endAt', $endAt->format('Y-m-d'))
                     ->setParameter('station', $station)
                     ->getQuery()
                     ->getResult();

        foreach ($rows as $row) {
            $data = array_map('intval', $row);
            $index = $data['equipmentId'];
            $equipmentInventory[$index] = $data;
        }

        return $equipmentInventory;
    }
}
