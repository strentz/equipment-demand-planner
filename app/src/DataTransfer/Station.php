<?php

declare(strict_types=1);

namespace App\DataTransfer;

use App\DataTransfer\Station\Campervan;
use App\DataTransfer\Station\Equipment;
use JsonSerializable;

class Station implements JsonSerializable
{
    private int $id;
    private string $name;
    private array $equipment;
    private array $campervans;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->equipment = [];
        $this->campervans = [];
    }

    public function setName(string $name): Station
    {
        $this->name = $name;
        return $this;
    }

    public function addEquipment(Equipment $equipment): Station
    {
        $this->equipment[] = $equipment;
        return $this;
    }

    public function addCampervan(Campervan $campervan): Station
    {
        $this->campervans[] = $campervan;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'equipment' => $this->equipment,
            'campervans' => $this->campervans
        ];
    }
}
