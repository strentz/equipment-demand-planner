<?php

declare(strict_types=1);

namespace App\DataTransfer\Station;

use JsonSerializable;

class Campervan implements JsonSerializable
{
    private string $name;
    private int $quantity;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     *
     * @return Equipment
     */
    public function setQuantity(int $quantity): Campervan
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'quantity' => $this->quantity
        ];
    }
}
