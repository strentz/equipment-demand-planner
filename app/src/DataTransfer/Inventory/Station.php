<?php

declare(strict_types=1);

namespace App\DataTransfer\Inventory;

use JsonSerializable;

class Station implements JsonSerializable
{
    private string $name;
    private array $equipment;
    private array $campervans;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addEquipment(Equipment $equipment): Station
    {
        $this->equipment[] = $equipment;
        return $this;
    }

    public function addCampervan(Campervan $campervan): Station
    {
        $this->campervans[] = $campervan;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEquipment(): array
    {
        return $this->equipment;
    }

    public function getCampervans(): array
    {
        return $this->campervans;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'equipment' => $this->equipment,
            'campervans' => $this->campervans
        ];
    }
}
