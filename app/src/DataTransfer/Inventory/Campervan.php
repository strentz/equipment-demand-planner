<?php

declare(strict_types=1);

namespace App\DataTransfer\Inventory;

use JsonSerializable;

class Campervan implements JsonSerializable
{
    private string $name;
    private int $availableQuantity;
    private int $bookedQuantity;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function setAvailableQuantity(int $availableQuantity): Campervan
    {
        $this->availableQuantity = $availableQuantity;
        return $this;
    }

    public function setBookedQuantity(int $bookedQuantity): Campervan
    {
        $this->bookedQuantity = $bookedQuantity;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAvailableQuantity(): int
    {
        return $this->availableQuantity;
    }

    public function getBookedQuantity(): int
    {
        return $this->bookedQuantity;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'bookedQuantity' => $this->bookedQuantity,
            'availableQuantity' => $this->availableQuantity,
        ];
    }
}
