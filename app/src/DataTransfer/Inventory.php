<?php

declare(strict_types=1);

namespace App\DataTransfer;

use App\DataTransfer\Inventory\Station;
use JsonSerializable;

class Inventory implements JsonSerializable
{
    private array $stations;

    public function __construct()
    {
        $this->stations = [];
    }

    public function addStation(Station $station): Inventory
    {
        $this->stations[] = $station;
        return $this;
    }

    public function getStations(): array
    {
        return $this->stations;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'stations' => $this->stations
        ];
    }
}
