<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Booking\BookingEquipment;
use App\Repository\BookingRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 * @ORM\Table(name="booking", indexes={
 *     @ORM\Index(name="start_at_idx", columns={"start_at"}),
 *     @ORM\Index(name="end_at_idx", columns={"end_at"})
 * })
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Campervan::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Campervan $campervan;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Station $origin;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Station $destination;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTimeInterface $startAt;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTimeInterface $endAt;

    /**
     * @ORM\OneToMany(targetEntity=BookingEquipment::class, mappedBy="booking", orphanRemoval=true)
     */
    private $bookingEquipment;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->bookingEquipment = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getOrigin(): ?Station
    {
        return $this->origin;
    }

    public function setOrigin(?Station $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getDestination(): ?Station
    {
        return $this->destination;
    }

    public function setDestination(?Station $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getStartAt(): ?DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return Collection|BookingItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return Collection|BookingEquipment[]
     */
    public function getBookingEquipment(): Collection
    {
        return $this->bookingEquipment;
    }

    public function addBookingEquipment(BookingEquipment $bookingEquipment): self
    {
        if (!$this->bookingEquipment->contains($bookingEquipment)) {
            $this->bookingEquipment[] = $bookingEquipment;
            $bookingEquipment->setBooking($this);
        }

        return $this;
    }

    public function removeBookingEquipment(BookingEquipment $bookingEquipment): self
    {
        if ($this->bookingEquipment->removeElement($bookingEquipment)) {
            // set the owning side to null (unless already changed)
            if ($bookingEquipment->getBooking() === $this) {
                $bookingEquipment->setBooking(null);
            }
        }

        return $this;
    }
}
