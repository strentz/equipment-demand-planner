<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\EquipmentInventoryRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EquipmentInventoryRepository::class, readOnly=true)
 * @ORM\Table(name="view_equipment_inventory")
 */
class EquipmentInventory
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Station $station;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Equipment::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Equipment $equipment;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $forecastAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $inboundQuantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $outboundQuantity;

    /**
     * Get station.
     *
     * @return Station|null
     */
    public function getStation(): ?Station
    {
        return $this->station;
    }

    /**
     * Set station.
     *
     * @param Station|null $station
     *
     * @return EquipmentInventory
     */
    public function setStation(?Station $station): EquipmentInventory
    {
        $this->station = $station;
        return $this;
    }

    /**
     * Get equipment.
     *
     * @return Equipment|null
     */
    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    /**
     * Set equipment.
     *
     * @param Equipment|null $equipment
     *
     * @return EquipmentInventory
     */
    public function setEquipment(?Equipment $equipment): EquipmentInventory
    {
        $this->equipment = $equipment;
        return $this;
    }

    /**
     * Get forecastAt.
     *
     * @return DateTimeInterface|null
     */
    public function getForecastAt(): ?DateTimeInterface
    {
        return DateTime::createFromFormat('Y-m-d', $this->forecastAt);
    }

    /**
     * Set forecastAt.
     *
     * @param DateTimeInterface|null $forecastAt
     *
     * @return EquipmentInventory
     */
    public function setForecastAt(?DateTimeInterface $forecastAt): EquipmentInventory
    {
        $this->forecastAt = $forecastAt;
        return $this;
    }

    /**
     * Get inboundQuantity.
     *
     * @return mixed
     */
    public function getInboundQuantity()
    {
        return $this->inboundQuantity;
    }

    /**
     * Set inboundQuantity.
     *
     * @param mixed $inboundQuantity
     *
     * @return EquipmentInventory
     */
    public function setInboundQuantity($inboundQuantity)
    {
        $this->inboundQuantity = $inboundQuantity;
        return $this;
    }

    /**
     * Get outboundQuantity.
     *
     * @return mixed
     */
    public function getOutboundQuantity()
    {
        return $this->outboundQuantity;
    }

    /**
     * Set outboundQuantity.
     *
     * @param mixed $outboundQuantity
     *
     * @return EquipmentInventory
     */
    public function setOutboundQuantity($outboundQuantity)
    {
        $this->outboundQuantity = $outboundQuantity;
        return $this;
    }
}
