<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Station\StationCampervan;
use App\Entity\Station\StationEquipment;
use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\OneToMany(targetEntity=StationEquipment::class, mappedBy="station", orphanRemoval=true)
     */
    private $stationEquipment;

    /**
     * @ORM\OneToMany(targetEntity=StationCampervan::class, mappedBy="station", orphanRemoval=true)
     */
    private $stationCampervans;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->stationEquipment = new ArrayCollection();
        $this->stationCampervans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|StationEquipment[]
     */
    public function getStationEquipment(): Collection
    {
        return $this->stationEquipment;
    }

    public function addStationEquipment(StationEquipment $stationEquipment): self
    {
        if (!$this->stationEquipment->contains($stationEquipment)) {
            $this->stationEquipment[] = $stationEquipment;
            $stationEquipment->setStation($this);
        }

        return $this;
    }

    public function removeStationEquipment(StationEquipment $stationEquipment): self
    {
        if ($this->stationEquipment->removeElement($stationEquipment)) {
            // set the owning side to null (unless already changed)
            if ($stationEquipment->getStation() === $this) {
                $stationEquipment->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StationCampervan[]
     */
    public function getStationCampervans(): Collection
    {
        return $this->stationCampervans;
    }

    public function addStationCampervan(StationCampervan $stationCampervan): self
    {
        if (!$this->stationCampervans->contains($stationCampervan)) {
            $this->stationCampervans[] = $stationCampervan;
            $stationCampervan->setStation($this);
        }

        return $this;
    }

    public function removeStationCampervan(StationCampervan $stationCampervan): self
    {
        if ($this->stationCampervans->removeElement($stationCampervan)) {
            // set the owning side to null (unless already changed)
            if ($stationCampervan->getStation() === $this) {
                $stationCampervan->setStation(null);
            }
        }

        return $this;
    }
}
