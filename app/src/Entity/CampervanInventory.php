<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CampervanInventoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CampervanInventoryRepository::class, readOnly=true)
 * @ORM\Table(name="view_campervan_inventory")
 */
class CampervanInventory
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Campervan::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $campervan;

    /**
     * @ORM\Id()
     * @ORM\Column(type="date")
     */
    private $forecastAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $inboundQuantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $outboundQuantity;

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getForecastAt(): ?\DateTimeInterface
    {
        return $this->forecastAt;
    }

    public function setForecastAt(\DateTimeInterface $forecastAt): self
    {
        $this->forecastAt = $forecastAt;

        return $this;
    }

    public function getInboundQuantity(): ?int
    {
        return $this->inboundQuantity;
    }

    public function setInboundQuantity(int $inboundQuantity): self
    {
        $this->inboundQuantity = $inboundQuantity;

        return $this;
    }

    public function getOutboundQuantity(): ?int
    {
        return $this->outboundQuantity;
    }

    public function setOutboundQuantity(int $outboundQuantity): self
    {
        $this->outboundQuantity = $outboundQuantity;

        return $this;
    }
}
