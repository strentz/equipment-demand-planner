<?php

declare(strict_types=1);

namespace App\Entity\Station;

use App\Entity\Campervan;
use App\Entity\Station;
use App\Repository\Station\StationCampervanRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StationCampervanRepository::class)
 */
class StationCampervan
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="stationCampervans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity=Campervan::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $campervan;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
