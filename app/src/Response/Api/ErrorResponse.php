<?php

declare(strict_types=1);

namespace App\Response\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorResponse extends JsonResponse
{
    public function __construct($error, int $status = 400, array $headers = [])
    {
        $data = [
            'error' => [
                'status' => $status,
                'message' => $error
            ]
        ];

        parent::__construct($data, $status, $headers, false);
    }
}
