<?php

declare(strict_types=1);

namespace App\Response\Api;

use Symfony\Component\HttpFoundation\JsonResponse;

class SuccessResponse extends JsonResponse
{
    public function __construct($data, int $status = 200, array $headers = [])
    {
        $data = [
            'data' => $data
        ];

        parent::__construct($data, $status, $headers, false);
    }
}
