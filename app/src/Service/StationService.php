<?php

declare(strict_types=1);

namespace App\Service;

use App\DataTransfer\Station;
use App\Repository\StationRepository;
use Doctrine\Common\Collections\Collection;

class StationService
{
    private StationRepository $repository;

    public function __construct(StationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return Station[]
     */
    public function getStations(): array
    {
        $stationsTransfer = [];

        foreach ($this->repository->findAll() as $station) {
            $stationTransfer = new Station($station->getId());
            $stationTransfer->setName($station->getName());

            $this->addEquipments($stationTransfer, $station->getStationEquipment());
            $this->addCampervans($stationTransfer, $station->getStationCampervans());

            $stationsTransfer[] = $stationTransfer;
        }

        return $stationsTransfer;
    }

    private function addEquipments(Station $stationTransfer, Collection $stationEquipments): void
    {
        foreach ($stationEquipments as $stationEquipment) {
            $equipmentTransfer = new Station\Equipment($stationEquipment->getEquipment()->getName());
            $equipmentTransfer->setQuantity($stationEquipment->getQuantity());
            $stationTransfer->addEquipment($equipmentTransfer);
        }
    }

    private function addCampervans(Station $stationTransfer, Collection $stationCampervans): void
    {
        foreach ($stationCampervans as $stationCampervan) {
            $campervanTransfer = new Station\Campervan($stationCampervan->getCampervan()->getName());
            $campervanTransfer->setQuantity($stationCampervan->getQuantity());
            $stationTransfer->addCampervan($campervanTransfer);
        }
    }
}
