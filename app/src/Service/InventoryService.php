<?php

declare(strict_types=1);

namespace App\Service;

use App\DataTransfer\Inventory;
use App\Entity\Station;
use App\Repository\CampervanInventoryRepository;
use App\Repository\EquipmentInventoryRepository;
use App\Validator\ValidationException;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;

class InventoryService
{
    public const LIMIT_DAYS = 365;

    private EquipmentInventoryRepository $equipmentInventoryRepository;
    private CampervanInventoryRepository $campervanInventoryRepository;

    public function __construct(
        EquipmentInventoryRepository $equipmentInventoryRepository,
        CampervanInventoryRepository $campervanInventoryRepository
    ) {
        $this->equipmentInventoryRepository = $equipmentInventoryRepository;
        $this->campervanInventoryRepository = $campervanInventoryRepository;
    }

    public function calculateInventory(array $stations, DateTimeInterface $forecastAt): Inventory
    {
        $startAt = new DateTimeImmutable('today');
        $limitAt = new DateTimeImmutable('+ ' . self::LIMIT_DAYS . ' days');

        if ($forecastAt < $startAt) {
            throw new ValidationException('To forecast inventory the minium date is the present day.');
        }

        if ($forecastAt > $limitAt) {
            throw new ValidationException('Maximum forecast range is ' . self::LIMIT_DAYS . ' days.');
        }

        $inventoryTransfer = new Inventory();

        foreach ($stations as $station) {
            $stationInventory = $this->calculateStationInventory($station, $startAt, $forecastAt);
            $inventoryTransfer->addStation($stationInventory);
        }

        return $inventoryTransfer;
    }

    private function calculateStationInventory(
        Station $station,
        DateTimeInterface $startAt,
        DateTimeInterface $forecastAt
    ): Inventory\Station {
        $stationTransfer = new Inventory\Station($station->getName());
        $equipmentInventory = $this->equipmentInventoryRepository->findBetweenRange($station, $startAt, $forecastAt);
        $campervanInventory = $this->campervanInventoryRepository->findBetweenRange($station, $startAt, $forecastAt);

        $this->addEquipment($stationTransfer, $station->getStationEquipment(), $equipmentInventory);
        $this->addCampervans($stationTransfer, $station->getStationCampervans(), $campervanInventory);

        return $stationTransfer;
    }

    private function addEquipment(
        Inventory\Station $stationTransfer,
        Collection $stationEquipments,
        array $equipmentInventory
    ): void {
        foreach ($stationEquipments as $stationEquipment) {
            $equipment = $stationEquipment->getEquipment();
            $index = $equipment->getId();

            $inbound = $equipmentInventory[$index]['inboundQuantity'] ?? 0;
            $outbound = $equipmentInventory[$index]['outboundQuantity'] ?? 0;
            $availableQuantity = $inbound - $outbound + $stationEquipment->getQuantity();

            $equipmentTransfer = new Inventory\Equipment($equipment->getName());
            $equipmentTransfer->setBookedQuantity($outbound);
            $equipmentTransfer->setAvailableQuantity($availableQuantity);

            $stationTransfer->addEquipment($equipmentTransfer);
        }
    }

    private function addCampervans(
        Inventory\Station $stationTransfer,
        Collection $stationCampervans,
        array $campervanInventory
    ): void {
        foreach ($stationCampervans as $stationCampervan) {
            $campervan = $stationCampervan->getCampervan();
            $index = $campervan->getId();

            $inbound = $campervanInventory[$index]['inboundQuantity'] ?? 0;
            $outbound = $campervanInventory[$index]['outboundQuantity'] ?? 0;
            $availableQuantity = $inbound - $outbound + $stationCampervan->getQuantity();

            $campervanTransfer = new Inventory\Campervan($campervan->getName());
            $campervanTransfer->setBookedQuantity($outbound);
            $campervanTransfer->setAvailableQuantity($availableQuantity);

            $stationTransfer->addCampervan($campervanTransfer);
        }
    }
}
