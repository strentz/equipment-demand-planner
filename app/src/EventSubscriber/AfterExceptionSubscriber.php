<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Response\Api\ErrorResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AfterExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AfterExceptionSubscriber constructor.
     *
     * @param LoggerInterface $logger Logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Catches application exceptions.
     *
     * @param ExceptionEvent $event Event
     *
     * @return void
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $uri = $event->getRequest()->getRequestUri();
        $message = $exception->getMessage();
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof HttpException) {
            $status = $exception->getStatusCode();
        }

        $this->logger->error($exception->getMessage(), [
            'trace' => $exception->getTraceAsString()
        ]);

        /**
         * I know, is not pretty. But just for simplicity in this case study.
         * Alternatively we could use different kernels one for the api and for the dashboard itesef.
         *
         * @see https://symfony.com/doc/current/configuration/multiple_kernels.html
         */
        if (preg_match('#^/api#', $uri)) {
            $response = new ErrorResponse($message, $status);
        } else {
            $response = new Response($message, $status);
        }

        $event->setResponse($response);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }
}
