<?php

declare(strict_types=1);

namespace App\Tests\Application;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class ApplicationTestCase extends WebTestCase
{
    protected KernelBrowser $client;

    protected const TABLES = [
        'booking_equipment',
        'booking',
        'station_equipment',
        'station_campervan',
        'station',
        'equipment',
        'campervan',
    ];

    protected function createFixtures(): void
    {
        $manager = self::$container->get('doctrine')->getManager();
        $fixtures = new Fixtures($manager);
        $fixtures->load();
    }

    /**
     * @inheritDoc
     */
    protected function tearDown(): void
    {
        $doctrine = self::$container->get('doctrine')->getManager();
        $connection = $doctrine->getConnection();

        foreach (self::TABLES as $table) {
            $connection->exec("DELETE FROM $table");
            $connection->exec("ALTER TABLE $table AUTO_INCREMENT = 1");
        }

        $doctrine->close();

        parent::tearDown();
    }
}
