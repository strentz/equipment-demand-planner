<?php

namespace App\Tests\Application;

use App\Entity\Booking;
use App\Entity\Campervan;
use App\Entity\Equipment;
use App\Entity\Station;
use Doctrine\ORM\EntityManagerInterface;
use DateTime;

class Fixtures
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @inheritDoc
     */
    public function load()
    {
        $campervans = $this->createCampervans();
        $equipments = $this->createEquipments();
        $stations = $this->createStations($campervans, $equipments);

        $booking = new Booking();
        $booking->setOrigin($stations[0]);
        $booking->setDestination($stations[1]);
        $booking->setCampervan($campervans[0]);
        $booking->setStartAt(new DateTime('+1 day'));
        $booking->setEndAt(new DateTime('+3 day'));

        $this->manager->persist($booking);

        $bookingEquipment = new Booking\BookingEquipment();
        $bookingEquipment->setBooking($booking);
        $bookingEquipment->setEquipment($equipments[0]);
        $bookingEquipment->setQuantity(5);

        $this->manager->persist($bookingEquipment);

        $bookingEquipment = new Booking\BookingEquipment();
        $bookingEquipment->setBooking($booking);
        $bookingEquipment->setEquipment($equipments[1]);
        $bookingEquipment->setQuantity(10);

        $this->manager->persist($bookingEquipment);

        $booking = new Booking();
        $booking->setOrigin($stations[1]);
        $booking->setDestination($stations[1]);
        $booking->setCampervan($campervans[1]);
        $booking->setStartAt(new DateTime('-1 day'));
        $booking->setEndAt(new DateTime('+2 day'));

        $this->manager->persist($booking);

        $bookingEquipment = new Booking\BookingEquipment();
        $bookingEquipment->setBooking($booking);
        $bookingEquipment->setEquipment($equipments[1]);
        $bookingEquipment->setQuantity(10);

        $this->manager->persist($bookingEquipment);

        $this->manager->flush();
        $this->manager->clear();
    }

    /**
     * @return Campervan[]
     */
    private function createCampervans(): array
    {
        $names = ['Surfer Suite', 'Travel Home'];
        $campervans = [];

        foreach ($names as $name) {
            $campervan = new Campervan();
            $campervan->setName($name);
            $this->manager->persist($campervan);

            $campervans[] = $campervan;
        }

        return $campervans;
    }

    /**
     * @return Equipment[]
     */
    private function createEquipments(): array
    {
        $names = ['Table', 'Chair'];
        $equipments = [];

        foreach ($names as $name) {
            $equipment = new Equipment();
            $equipment->setName($name);
            $this->manager->persist($equipment);

            $equipments[] = $equipment;
        }

        return $equipments;
    }

    /**
     * @return Station[]
     */
    private function createStations(array $campervans, array $equipments): array
    {
        $names = ['Station A', 'Station B'];
        $quantities = [5, 10];
        $stations = [];

        foreach ($names as $name) {
            $station = new Station();
            $station->setName($name);

            foreach ($campervans as $index => $campervan) {
                $stationCampervan = new Station\StationCampervan();
                $stationCampervan->setCampervan($campervan);
                $stationCampervan->setStation($station);
                $stationCampervan->setQuantity($quantities[$index]);

                $this->manager->persist($stationCampervan);
            }

            foreach ($equipments as $index => $equipment) {
                $stationEquipment = new Station\StationEquipment();
                $stationEquipment->setEquipment($equipment);
                $stationEquipment->setStation($station);
                $stationEquipment->setQuantity($quantities[$index]);

                $this->manager->persist($stationEquipment);
            }

            $this->manager->persist($station);

            $stations[] = $station;
        }

        return $stations;
    }
}
