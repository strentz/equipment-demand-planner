<?php

declare(strict_types=1);

namespace App\Tests\Application\Controller\Dashboard;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class IndexControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request('GET', '/dashboard');

        $this->assertResponseRedirects('/dashboard/' . date('Y-m-d'));
    }

    public function testDashboard(): void
    {
        $client = static::createClient();
        $client->request('GET', '/dashboard/' . date('Y-m-d'));

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'text/html; charset=UTF-8');
    }

    public function testDashboardNotFound(): void
    {
        $client = static::createClient();
        $client->request('GET', '/dashboard/2020-02-31');

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
        $this->assertResponseHeaderSame('Content-Type', 'text/html; charset=UTF-8');

        $client->request('GET', '/dashboard/' . date('Y-m-d', strtotime('-1 day')));

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
        $this->assertResponseHeaderSame('Content-Type', 'text/html; charset=UTF-8');
    }
}
