<?php

declare(strict_types=1);

namespace App\Tests\Application\Controller\Api;

use App\Service\InventoryService;
use App\Tests\Application\ApplicationTestCase;
use Symfony\Component\HttpFoundation\Response;

class InventoryControllerTest extends ApplicationTestCase
{
    /**
     * @dataProvider inventoryByDateDataProvider
     */
    public function testInventoryGet(string $date, string $jsonFile): void
    {
        $client = static::createClient();
        $this->createFixtures();

        $client->request('GET', '/api/inventory/' . $date);
        $content = $client->getResponse()->getContent();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJsonStringEqualsJsonFile($jsonFile, $content);
    }

    public function inventoryByDateDataProvider(): array
    {
        $data = [];

        for ($i = 0; $i < 5; $i++) {
            $date = date('Y-m-d', strtotime($i . ' day'));
            $data[] = [$date, __DIR__ . "/Responses/InventoryControllerGet-$i.json"];
        }

        return $data;
    }

    public function testInventoryGetHasInvalidDate(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/inventory/2021-02-34');
        $content = $client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJsonStringEqualsJsonFile(__DIR__ . '/Responses/InventoryControllerGetInvalidDate.json', $content);
    }

    public function testInventoryGetNotFound(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/inventory/777');
        $content = $client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJsonStringEqualsJsonFile(__DIR__ . '/Responses/InventoryControllerGetNotFound.json', $content);
    }

    public function testInventoryGetLimitExceed(): void
    {
        $client = static::createClient();

        $exceedDays =  date('Y-m-d', strtotime('+' . (InventoryService::LIMIT_DAYS + 1) . ' day'));
        $client->request('GET', '/api/inventory/' . $exceedDays);
        $content = $client->getResponse()->getContent();

        $this->assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJsonStringEqualsJsonFile(__DIR__ . '/Responses/InventoryControllerGetLimitExceed.json', $content);
    }
}
