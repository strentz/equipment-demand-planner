<?php

declare(strict_types=1);

namespace App\Tests\Application\Controller\Api;

use App\Tests\Application\ApplicationTestCase;

class StationControllerTest extends ApplicationTestCase
{
    public function testStationsGetAll(): void
    {
        $client = static::createClient();
        $this->createFixtures();

        $client->request('GET', '/api/stations');
        $content = $client->getResponse()->getContent();

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertJsonStringEqualsJsonFile(__DIR__ . '/Responses/StationControllerGetAll.json', $content);
    }
}
