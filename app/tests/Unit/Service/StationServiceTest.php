<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service;

use App\Entity\Campervan;
use App\Entity\Equipment;
use App\Entity\Station;
use App\Repository\StationRepository;
use App\Service\StationService;
use PHPUnit\Framework\TestCase;
use App\DataTransfer;

class StationServiceTest extends TestCase
{
    public function testGetStations(): void
    {
        $station = $this->createStation();
        $stationTransfer = $this->createExpectedStationTransfer();

        $repository = $this->getMockBuilder(StationRepository::class)
                           ->disableOriginalConstructor()
                           ->onlyMethods(['findAll'])
                           ->getMock();

        $repository->expects($this->once())
                   ->method('findAll')
                   ->willReturn([$station]);

        $service = new StationService($repository);
        $stations = $service->getStations();

        $this->assertCount(1, $stations);
        $this->assertEquals($stationTransfer, $stations[0]);
    }

    private function createStation(): Station
    {
        $campervan = $this->createEntityMock(Campervan::class);
        $campervan->setName('Campervan A');

        $equipment = $this->createEntityMock(Equipment::class);
        $equipment->setName('Equipment A');

        $station = $this->createEntityMock(Station::class);
        $station->setName('Station A');

        $stationCampervan = new Station\StationCampervan();
        $stationCampervan->setCampervan($campervan);
        $stationCampervan->setStation($station);
        $stationCampervan->setQuantity(10);

        $stationEquipment = new Station\StationEquipment();
        $stationEquipment->setEquipment($equipment);
        $stationEquipment->setStation($station);
        $stationEquipment->setQuantity(20);

        $station->addStationCampervan($stationCampervan);
        $station->addStationEquipment($stationEquipment);

        return $station;
    }

    private function createExpectedStationTransfer(): DataTransfer\Station
    {
        $campervan = new  DataTransfer\Station\Campervan('Campervan A');
        $campervan->setQuantity(10);
        $equipment = new  DataTransfer\Station\Equipment('Equipment A');
        $equipment->setQuantity(20);

        $station = new DataTransfer\Station(1);
        $station->setName('Station A');
        $station->addCampervan($campervan);
        $station->addEquipment($equipment);

        return $station;
    }

    private function createEntityMock(string $class)
    {
        $mock = $this->getMockBuilder($class)
                     ->onlyMethods(['getId'])
                     ->getMock();

        $mock->expects($this->any())
                   ->method('getId')
                   ->willReturn(1);

        return $mock;
    }
}
