<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523131858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create initial schema structure';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, campervan_id INT NOT NULL, origin_id INT NOT NULL, destination_id INT NOT NULL, start_at DATE NOT NULL, end_at DATE NOT NULL, INDEX IDX_E00CEDDEB9D53E94 (campervan_id), INDEX IDX_E00CEDDE56A273CC (origin_id), INDEX IDX_E00CEDDE816C6140 (destination_id), INDEX start_at_idx (start_at), INDEX end_at_idx (end_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking_equipment (booking_id INT NOT NULL, equipment_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_400A1E803301C60 (booking_id), INDEX IDX_400A1E80517FE9FE (equipment_id), PRIMARY KEY(booking_id, equipment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campervan (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station_campervan (station_id INT NOT NULL, campervan_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_EA15D56421BDB235 (station_id), INDEX IDX_EA15D564B9D53E94 (campervan_id), PRIMARY KEY(station_id, campervan_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station_equipment (station_id INT NOT NULL, equipment_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_51BCBB9821BDB235 (station_id), INDEX IDX_51BCBB98517FE9FE (equipment_id), PRIMARY KEY(station_id, equipment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEB9D53E94 FOREIGN KEY (campervan_id) REFERENCES campervan (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE56A273CC FOREIGN KEY (origin_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE816C6140 FOREIGN KEY (destination_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE booking_equipment ADD CONSTRAINT FK_400A1E803301C60 FOREIGN KEY (booking_id) REFERENCES booking (id)');
        $this->addSql('ALTER TABLE booking_equipment ADD CONSTRAINT FK_400A1E80517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id)');
        $this->addSql('ALTER TABLE station_campervan ADD CONSTRAINT FK_EA15D56421BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE station_campervan ADD CONSTRAINT FK_EA15D564B9D53E94 FOREIGN KEY (campervan_id) REFERENCES campervan (id)');
        $this->addSql('ALTER TABLE station_equipment ADD CONSTRAINT FK_51BCBB9821BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE station_equipment ADD CONSTRAINT FK_51BCBB98517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking_equipment DROP FOREIGN KEY FK_400A1E803301C60');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEB9D53E94');
        $this->addSql('ALTER TABLE station_campervan DROP FOREIGN KEY FK_EA15D564B9D53E94');
        $this->addSql('ALTER TABLE booking_equipment DROP FOREIGN KEY FK_400A1E80517FE9FE');
        $this->addSql('ALTER TABLE station_equipment DROP FOREIGN KEY FK_51BCBB98517FE9FE');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE56A273CC');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE816C6140');
        $this->addSql('ALTER TABLE station_campervan DROP FOREIGN KEY FK_EA15D56421BDB235');
        $this->addSql('ALTER TABLE station_equipment DROP FOREIGN KEY FK_51BCBB9821BDB235');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_equipment');
        $this->addSql('DROP TABLE campervan');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE station');
        $this->addSql('DROP TABLE station_campervan');
        $this->addSql('DROP TABLE station_equipment');
    }
}
