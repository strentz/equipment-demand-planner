<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523131919 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create inventory views';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(/** @lang SQL */ <<<EOQ
            CREATE VIEW view_equipment_inventory AS
            SELECT
             I.station_id,
             I.equipment_id,
             I.forecast_at,
             SUM(I.inbound_quantity) AS inbound_quantity,
             SUM(I.outbound_quantity) AS outbound_quantity
            FROM
             (
             SELECT
              BE.equipment_id AS equipment_id,
              B.destination_id AS station_id,
              B.end_at AS forecast_at,
              SUM(BE.quantity) AS inbound_quantity,
              0 AS outbound_quantity
             FROM
              booking_equipment BE
             INNER JOIN booking B ON
              B.id = BE.booking_id
             GROUP BY
              B.destination_id ,
              B.end_at,
              BE.equipment_id
            UNION ALL
             SELECT
              BE.equipment_id AS equipment_id,
              B.origin_id AS station_id,
              B.start_at AS forecast_at,
              0 AS inbound_quantity,
              SUM(BE.quantity) AS outbound_quantity
             FROM
              booking_equipment BE
             INNER JOIN booking B ON
              B.id = BE.booking_id
             GROUP BY
              B.origin_id,
              B.start_at,
              BE.equipment_id) AS I
            GROUP BY
             I.equipment_id,
             I.station_id,
             I.forecast_at
            EOQ
        );

        $this->addSql(/** @lang SQL */ <<<EOQ
            CREATE VIEW view_campervan_inventory AS
            SELECT
             I.station_id,
             I.campervan_id,
             I.forecast_at,
             SUM(I.inbound_quantity) AS inbound_quantity,
             SUM(I.outbound_quantity) AS outbound_quantity
            FROM
             (
             SELECT
              B.campervan_id AS campervan_id,
              B.destination_id AS station_id,
              B.end_at AS forecast_at,
              COUNT(B.campervan_id) AS inbound_quantity,
              0 AS outbound_quantity
             FROM
              booking B
             GROUP BY
              B.destination_id ,
              B.end_at,
              B.campervan_id
            UNION ALL
             SELECT
              B.campervan_id AS campervan_id,
              B.origin_id AS station_id,
              B.start_at AS forecast_at,
              0 AS inbound_quantity,
              COUNT(B.campervan_id) AS outbound_quantity
             FROM
              booking B
             GROUP BY
              B.origin_id,
              B.start_at,
              B.campervan_id) AS I
            GROUP BY
             I.station_id,
             I.forecast_at,
             I.campervan_id
            EOQ
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang SQL */'DROP VIEW view_equipment_inventory');
        $this->addSql(/** @lang SQL */'DROP VIEW view_campervan_inventory');
    }
}
