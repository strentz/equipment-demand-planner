#!/usr/bin/env bash

echo -e "\033[1;32mBuilding and starting containers... It might take 5 minutes or so ;) \033[0m"
docker-compose up -d
docker-compose exec app composer install --optimize-autoloader
docker-compose exec app composer dump-env dev > /dev/null 2>&1
echo -e "\033[1;32mCreating database...\033[0m"
docker-compose exec app php bin/console --no-interaction doctrine:database:drop --force --if-exists > /dev/null 2>&1
docker-compose exec app php bin/console --no-interaction doctrine:database:create --if-not-exists > /dev/null 2>&1
echo -e "\033[1;32mRunnng database migrations...\033[0m"
docker-compose exec app php bin/console --no-interaction doctrine:migrations:migrate > /dev/null 2>&1
./tests.sh
docker-compose exec app bin/console cache:clear > /dev/null 2>&1
echo -e "\033[1;32mGenerating demo fixtures...\033[0m"
docker-compose exec app php bin/console doctrine:fixtures:load --no-interaction > /dev/null 2>&1
echo -e "\033[1;32mSetup completed.\033[0m"
echo -e "\033[1;32mYou can open:\033[0m http://127.0.0.1"