# README #

### Programming Task: **Road Surfer**

**Name:** Raul Francisco Vilgre La Madrid

**Email:** f.lamadrid@icloud.com

#### Description
The application EDP (Equipment Demand Planner) consists of an API and a frontend page (dashboard).   

#### Considerations on the task:
I would like to clarify some reasoning since the task leaves many things to interpretation.  
We assume that the stations knows about all type of equipment, and it has a specific stock of each at the current date.

**The API itself consist of 2 REST endpoints.**      
`/api/stations`: Get a list of stations with current equipment and campervan stock.

`/api/inventory/{date}`: Get the inventory of each station at given point in date calculating the booking orders between current date and the requested one. 
The booking items for that given day is discounted from the availability already.

I imagined the client frontend could have more flexibility by having an endpoint with station specific data.
I created an index page to access the endpoints and the dashboard itself easily.
The task requested the format also available as HTML. I was not sure if it means literally JSON -> HTML or the dashboard iself.
I decided to create the dashboard timeline that is browsable by date.

#### Extra:
* Is not required by the task description explicity, 
  but I thought is nice knowing the amount of campervans of each model on each station too.

##### Some things can have been done better:
* Increase code coverage.
* Docker image is only for development.
* Depending on the use case performance can be improved.
* Offer an endpoint to display openapi spec.
* JWT Authentication for the API?

#### Requirements:
* Docker Compose. if you don't have it already: https://www.docker.com/get-started
* Port 80 available.

#### Instructions
1. Install [docker/docker-composer](https://www.docker.com/get-started) if you don't have it already.
2. Run `./start.sh` (It can take around 5 minutes)
3. Open [localhost](http://127.0.0.1) to browse it.
4. To run only tests `./tests.sh`
5. Code coverage report is available within `./app/var/coverage/index.html`
6. In case you need any help you can contact me at _f.lamadrid@icloud.com_

#### Toubleshoot
* If errors during start, this might be related to mounting docker volumes, but it depends on the host operative system.
  On Mac allow access, to the running directory by going to:   
  Preferences -> Security & Privacy -> Privacy -> Select Files & Folders -> Check the folder used by docker.

#### Stack:
* MySQL 8.0
* PHP 7.4
* Symfony 5.2
* The setup was tested on Mac only.