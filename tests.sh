#!/usr/bin/env bash

echo -e "\033[1;32mRunnng tests...\033[0m"
docker-compose exec app bin/console --no-interaction doctrine:database:drop --if-exists --force --env=test > /dev/null 2>&1
docker-compose exec app bin/console --no-interaction doctrine:database:create --if-not-exists --env=test > /dev/null 2>&1
docker-compose exec app bin/console --no-interaction doctrine:migrations:migrate --env=test > /dev/null 2>&1
docker-compose exec app bin/console cache:clear --env=test > /dev/null 2>&1
docker-compose exec app bin/phpunit
