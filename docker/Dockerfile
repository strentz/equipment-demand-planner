# -----------------------------------------------------------------------
# ----                      BASE PHP IMAGE (DEV)                     ----
# -----------------------------------------------------------------------
FROM php:7.4-fpm-buster

MAINTAINER Francisco La Madrid <f.lamadrid@icloud.com>

ENV DEBIAN_FRONTEND noninteractive

# ---------------------- Argument initialization -------------------------
# User variables
ARG DOCKER_USER
ARG DOCKER_GROUP

# Dependencies configuration
ARG PHPCONF_PATH=/usr/local/etc/php/conf.d
# -----------------------------------------------------------------------

# ---------------------- Docker user configutation ----------------------
# Links required for non-root users
RUN ln -fsn /usr/local/bin/php /usr/bin/php
RUN ln -fsn /usr/local/bin/php-config /usr/bin/php-config

# Create a system group and a system user and add to the newly created group
# The user has also a specified uuid -> 1000
RUN groupadd -r ${DOCKER_GROUP} && useradd -ru 1000 ${DOCKER_USER} -g ${DOCKER_GROUP}
# -----------------------------------------------------------------------

# ---------------------- Required build packages ------------------------
RUN buildDeps=" \
        libicu-dev \
        libpq-dev \
        zlib1g-dev \
        libzip-dev \
        libonig-dev \
    " \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        $buildDeps \
        libzip4 \
        unzip \
        git \
    && docker-php-ext-install pcntl bcmath opcache intl zip pdo_mysql mbstring \
    && pecl install APCu apcu_bc \
    && echo "extension=apcu.so" >> /usr/local/etc/php/conf.d/apcu.ini \
    && echo "apc.enabled=1" >> /usr/local/etc/php/conf.d/apcu.ini \
    && echo "apc.enable_cli=1" >> /usr/local/etc/php/conf.d/apcu.ini \
    && pecl clear-cache \
    && docker-php-ext-enable apc opcache \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false $buildDeps \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ----------------------- PHP configurations ----------------------------
COPY docker/php/global.conf /usr/local/etc/php-fpm.d/global.conf
COPY docker/php/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY docker/php/php.ini /usr/local/etc/php/conf.d/php-custom.ini
# -----------------------------------------------------------------------

# ---------------------- Composer configuration -------------------------
RUN chown -R ${DOCKER_USER}:${DOCKER_GROUP} /usr/local/var/log

COPY --from=composer:2.0 /usr/bin/composer /bin/composer
RUN chmod +rx /bin/composer

RUN mkdir /home/edp
RUN mkdir /home/edp/.composer
RUN chown -R ${DOCKER_USER}:${DOCKER_GROUP} /home/edp

# -----------------------------------------------------------------------

# ---------------------- Development ------------------------------------
RUN mv /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini.ignore \
    && pecl install xdebug-3.0.4 \
    && echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && echo "xdebug.start_with_request=no" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && echo "xdebug.mode=coverage" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && echo "xdebug.log=/tmp/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini  \
    && docker-php-ext-enable xdebug
# -----------------------------------------------------------------------

WORKDIR /app

RUN chown ${DOCKER_USER}:${DOCKER_GROUP} -R /app
RUN chown ${DOCKER_USER}:${DOCKER_GROUP} -R /home/edp/.composer

USER ${DOCKER_USER}

CMD ["php-fpm"]